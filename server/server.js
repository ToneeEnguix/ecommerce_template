require("dotenv").config("./.env");
const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const port = process.env.PORT || 8080;

const my_first_route = require("./routes/my_first_route");
const payments = require("./routes/payments");

const app = express();

app.use(express());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

(async () => {
  try {
    await mongoose.connect(process.env.MONGO, {
      useFindAndModify: false,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    console.info("Connected to the DB");
  } catch (error) {
    console.info(
      "ERROR: Seems like your DB is not running, please start it up !!!",
      error
    );
  }
})();

app.use("/test", my_first_route);
app.use("/payments", payments);

app.listen(port, () => {
  console.info("Server listening on port " + port);
});
