const Items = require("../models/my_first_model");

class TodosController {
  async create(req, res) {
    const { item } = req.body;
    try {
      const newItem = await Items.create({ item });
      res.status(200).send({ ok: true, newItem });
    } catch (err) {
      console.info(err);
      res.status(400).send({ ok: false, message: "Something went wrong" });
    }
  }

  async read(req, res) {
    try {
      const items = await Items.find({});
      res.status(200).send({ ok: true, items });
    } catch (err) {
      console.info(err);
      res.status(400).send({ ok: false, message: "Something went wrong" });
    }
  }

  async update(req, res) {
    let { item, newItem } = req.body;
    try {
      const updated = await Items.updateOne({ item }, { item: newItem });
      res.status(200).send({ ok: true, updated });
    } catch (err) {
      console.info(err);
      res.status(400).send({ ok: false, message: "Something went wrong" });
    }
  }

  async delete(req, res) {
    const { item } = req.body;
    try {
      const removed = await Items.deleteOne({ item });
      res.status(200).send({ ok: true, removed });
    } catch (err) {
      console.info(err);
      res.status(400).send({ ok: false, message: "Something went wrong" });
    }
  }
}
module.exports = new TodosController();
