const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);

const calculateOrderAmount = (items) => {
  let total = 0;
  items.forEach((item) => {
    total += item.price * item.amount;
  });
  // total in cents
  return total * 100;
};

class paymentsController {
  async create_checkout_session(req, res) {
    const { items } = req.body;
    try {
      const paymentIntent = await stripe.paymentIntents.create({
        amount: calculateOrderAmount(items),
        currency: "eur",
      });
      res.status(200).send({
        ok: true,
        clientSecret: paymentIntent.client_secret,
      });
    } catch (err) {
      console.info(err);
      res.status(400).send({ ok: false, message: "Something wrong happened" });
    }
  }
}

module.exports = new paymentsController();
