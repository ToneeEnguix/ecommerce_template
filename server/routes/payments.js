const express = require("express");
const router = express.Router();
const controller = require("../controllers/payments");

router.post("/create_checkout_session", controller.create_checkout_session);

module.exports = router;
