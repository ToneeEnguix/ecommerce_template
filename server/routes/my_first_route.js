const express = require("express");
const router = express.Router();
const controller = require("../controllers/my_first_controller");

// This is called a CRUD app: Create, Read, Update, Delete

router.post("/create", controller.create); // C

router.get("/read", controller.read); // R

router.post("/update", controller.update); // U

router.post("/delete", controller.delete); // D

module.exports = router;
