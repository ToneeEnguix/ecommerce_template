### To start the project
1- Clone the repo\
2- Access the repo `cd ecommerce_template`\
3- Install dependencies `npm i`\
4- Start client `npm start`\
5- Open new tab and go into server `CMD + T`+ `cd ../server`\
6- Install dependencies `npm i`\
7- Start server `nodemon`\
8- You will have to create .env file and add the following values:
- MONGO
- STRIPE_SECRET_KEY
- PORT (optional, will default to 8080)



### This repo contains:
- Cart saving in localStorage
- User authentication using @auth0/auth0-react (easy installing)
- Custom checkout
- Payment gateway using @stripe/stripe-js and @stripe/react-stripe-js
