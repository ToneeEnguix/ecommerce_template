/** @jsxRuntime classic /
/* @jsx jsx */
import { jsx } from "@emotion/react";
import Layout from "../components/Layout";
import { useRecoilValue } from "recoil";
import { products } from "../utils/atoms.js";
import { addToCart } from "../utils/cartFunctions";
import { useState } from "react";

const page = {
  minHeight: "500px",
  display: "flex",
  alignItems: "center",
  flexDirection: "column",
  paddingTop: "50px",
  h2: {
    marginBottom: "50px",
  },
};

const productsWrapper = {
  width: "80vw",
  display: "flex",
  flexWrap: "wrap",
};

const productStyle = {
  backgroundColor: "lightgray",
  width: "45%",
  display: "flex",
  alignItems: "center",
  flexDirection: "column",
  margin: "20px",
  p: { padding: "10px" },
};

export default function Home(props) {
  const productsDB = useRecoilValue(products);
  const [update, setUpdate] = useState(false);

  window.scrollTo(0, 0);

  return (
    <Layout {...props} update={update} setUpdate={() => setUpdate(false)}>
      <div css={page}>
        <h2>Featured Collection</h2>
        <div css={productsWrapper}>
          {productsDB
            ?.filter((product, i) => i < 4)
            .map((product, j) => {
              return (
                <div css={productStyle} key={product.id}>
                  <p>{product.title}</p>
                  <img
                    alt="product"
                    css={{ width: "100%" }}
                    src={product.imageUrl}
                  />
                  <p>{product.price}€</p>
                  <p
                    css={{
                      background: "lightgreen",
                      width: "100%",
                      textAlign: "center",
                      cursor: "pointer",
                      ":hover": {
                        fontWeight: "bold",
                      },
                    }}
                    onClick={() => {
                      setUpdate(true);
                      addToCart(product);
                    }}
                  >
                    Add to Cart
                  </p>
                </div>
              );
            })}
        </div>
      </div>
    </Layout>
  );
}
