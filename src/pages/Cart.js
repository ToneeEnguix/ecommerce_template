/** @jsxRuntime classic /
/* @jsx jsx */
import { jsx } from "@emotion/react";
import Layout from "../components/Layout";
import { URL } from "../config";
import axios from "axios";
import { useState, useEffect } from "react";
import { Redirect, Link } from "react-router-dom";
import { clearCart } from "../utils/cartFunctions";

const page = {
  minHeight: "500px",
  display: "flex",
  alignItems: "center",
  flexDirection: "column",
  paddingTop: "50px",
  h2: {
    marginBottom: "50px",
  },
};

export default function Cart() {
  const [sessionId, setSessionId] = useState(null);
  const [cart, setCart] = useState([]);
  const [update, setUpdate] = useState(false);

  useEffect(() => {
    window.scrollTo(0, 0);
    setCart(JSON.parse(localStorage.getItem("cart")));
  }, [update]);

  const createCheckoutSession = async () => {
    try {
      const res = await axios.post(`${URL}/payments/create_checkout_session`, {
        items: cart,
      });
      setSessionId(res.data.clientSecret);
    } catch (err) {
      console.error(err);
    }
  };
  if (sessionId) {
    return (
      <Redirect
        to={{
          pathname: `/checkout`,
          state: { sessionId },
        }}
      />
    );
  }
  return (
    <Layout>
      <div css={page}>
        <h2>Im Cart</h2>
        <div>
          {cart?.map((item) => {
            return (
              <div key={item.id}>
                <img alt="product" src={item.imageUrl} />
                <p>{item.title}</p>
                <p>{item.price * item.amount}€</p>
                <p>Quantity: {item.amount}</p>
              </div>
            );
          })}
        </div>
        {cart?.length > 0 ? (
          <div>
            <button
              onClick={() => {
                setUpdate(!update);
                clearCart();
              }}
            >
              Clear
            </button>

            <button onClick={createCheckoutSession}>Checkout</button>
          </div>
        ) : (
          <div>
            <p>No products yet!</p>
            <Link
              className="original"
              css={{ ":hover": { fontWeight: "bold" } }}
              to="/"
            >
              Go Home
            </Link>
          </div>
        )}
      </div>
    </Layout>
  );
}
