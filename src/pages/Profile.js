/** @jsxRuntime classic /
/* @jsx jsx */
import { jsx } from "@emotion/react";
import Layout from "../components/Layout";
import { useAuth0 } from "@auth0/auth0-react";

const page = {
  minHeight: "500px",
  display: "flex",
  alignItems: "center",
  flexDirection: "column",
  paddingTop: "50px",
  h2: {
    marginBottom: "50px",
  },
};

export default function Profile() {
  const { user } = useAuth0();
  return (
    <Layout>
      <div css={page}>
        <h2>Profile</h2>
        {user ? (
          <div>
            <img src={user.picture} alt={user.name} />
            <h3>{user.name}</h3>
            <p>Email: {user.email}</p>
          </div>
        ) : (
          <p>Loading...</p>
        )}
      </div>
    </Layout>
  );
}
