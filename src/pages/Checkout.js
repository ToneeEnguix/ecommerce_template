/** @jsxRuntime classic /
/* @jsx jsx */
import { jsx } from "@emotion/react";
import { CardElement, useStripe, useElements } from "@stripe/react-stripe-js";
import Layout from "../components/Layout";

const page = {
  minHeight: "500px",
  display: "flex",
  alignItems: "center",
  flexDirection: "column",
  paddingTop: "50px",
  h2: {
    marginBottom: "50px",
  },
};

const form = {
  minWidth: "400px",
  "*": {
    width: "100%",
  },
};

export default function Checkout(props) {
  const stripe = useStripe();
  const elements = useElements();

  const handleSubmit = async (event) => {
    event.preventDefault();
    stripe
      .confirmCardPayment(props.location.state.sessionId, {
        payment_method: {
          card: elements.getElement(CardElement),
        },
      })
      .then(function (result) {
        if (result.error) {
          // Show error to your customer
          // showError(result.error.message);
          console.error(result.error);
          // redirect to error page
        } else {
          // The payment succeeded!
          // orderComplete(result.paymentIntent.id);
          console.log(result);
          // redirect to success page
        }
      });
  };

  console.log(props);
  return (
    <Layout>
      <div css={page}>
        <h2>Checkout</h2>
        <form onSubmit={handleSubmit} css={form}>
          <CardElement />
          <button>Pay</button>
        </form>
      </div>
    </Layout>
  );
}
