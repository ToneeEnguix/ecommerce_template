const addToCart = async (product) => {
  const cart = JSON.parse(await localStorage.getItem("cart")) || [];
  let tempCart = [...cart];
  const foundIdx = cart.findIndex((item) => {
    return item.id === product.id;
  });
  if (foundIdx === -1) {
    product["amount"] = 1;
    tempCart.push(product);
  } else {
    tempCart[foundIdx].amount++;
  }
  localStorage.setItem("cart", JSON.stringify(tempCart));
};

const clearCart = () => {
  localStorage.removeItem("cart");
};

export { addToCart, clearCart };
