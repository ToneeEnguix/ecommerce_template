/** @jsxRuntime classic /
/** @jsx jsx */
import { jsx } from "@emotion/react";

const page = {
  display: "flex",
  alignItems: "center",
  justifyContent: "space-around",
  backgroundColor: "lightgray",
  height: "10vh",
  minHeight: "60px",
  marginTop: "100px",
};

export default function footer() {
  return <div css={page}>Im footer</div>;
}
