/** @jsxRuntime classic /
/* @jsx jsx */
import { jsx } from "@emotion/react";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useAuth0 } from "@auth0/auth0-react";

const page = {
  display: "flex",
  alignItems: "center",
  justifyContent: "space-around",
  backgroundColor: "lightblue",
  height: "10vh",
  minHeight: "60px",
  position: "fixed",
  width: "100%",
  div: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-around",
    "p, a": {
      marginRight: "20px",
      cursor: "pointer",
      textDecoration: "none",
      color: "inherit",
      display: "inline",
      ":hover": {
        color: "red",
      },
    },
  },
};

export default function Header(props) {
  const [cart, setCart] = useState(null);

  const { isAuthenticated, isLoading } = useAuth0();

  useEffect(() => {
    setCart(JSON.parse(localStorage.getItem("cart")));
    props.setUpdate && props.setUpdate();
  }, [props]);

  return (
    <div css={page}>
      <div>
        <p>Logo</p>
        <p>Brand</p>
      </div>
      <div>
        <Link to="/">Home</Link>
        <Link to="/cart">
          <p css={{ marginRight: "5px !important" }}>Cart</p>
          <span>{cart?.length}</span>
        </Link>
        {isLoading ? (
          <div>Loading...</div>
        ) : !isAuthenticated ? (
          <LoginButton />
        ) : (
          isAuthenticated && (
            <div>
              <Link to="/profile">Profile</Link>
              <LogoutButton />
            </div>
          )
        )}
      </div>
    </div>
  );
}

const LoginButton = () => {
  const { loginWithRedirect } = useAuth0();
  return <p onClick={loginWithRedirect}>Log in</p>;
};

const LogoutButton = () => {
  const { logout } = useAuth0();
  return (
    <p onClick={() => logout({ returnTo: window.location.origin })}>Log out</p>
  );
};
