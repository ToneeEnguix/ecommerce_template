/** @jsxRuntime classic /
/* @jsx jsx */
import { jsx } from "@emotion/react";
import Header from "./header";
import Footer from "./footer";

const Layout = (props) => {
  return (
    <div>
      <Header {...props} />
      <div css={{ paddingTop: "10vh" }}>{props.children}</div>
      <Footer />
    </div>
  );
};

export default Layout;
