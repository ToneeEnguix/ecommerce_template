import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter } from "react-router-dom";
import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";
import { STRIPE_PUBLISHABLE_KEY } from "./config.js";
import { RecoilRoot } from "recoil";
import "./app.css";
import { Auth0Provider } from "@auth0/auth0-react";

const stripePromise = loadStripe(STRIPE_PUBLISHABLE_KEY);

ReactDOM.render(
  <BrowserRouter>
    <Auth0Provider
      domain="webdevtoni.eu.auth0.com"
      clientId="s1LnL1wAQD7hRTUAMfze7WgeaS1WEy8a"
      redirectUri={window.location.origin}
    >
      <RecoilRoot>
        <Elements stripe={stripePromise}>
          <App />
        </Elements>
      </RecoilRoot>
    </Auth0Provider>
  </BrowserRouter>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
